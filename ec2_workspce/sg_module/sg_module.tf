variable "vpcid" {
    type=string
    default = "vpc-08192d60" #got form the aws console vpc=>myvpc
}
variable "sg_name" {}
resource "aws_security_group" "terraform_ec2_sg" {
  name        = "${var.sg_name}"
  description = "terraform sg ec2 instances"
  vpc_id      = "${var.vpcid}"

#it means allow port num 22 from everywhere,you  can also specify the IP address
#issh into this instance , use TCP protocol
  ingress {
    description      = "TLS from VPC"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
   
  }

  egress {
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]

  }
}

output "sg_id_output"{
    value = "${aws_security_group.terraform_ec2_sg.id}"
}