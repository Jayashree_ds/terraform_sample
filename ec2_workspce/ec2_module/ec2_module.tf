# provider "aws" {
#     region= "ca-central-1"
#     profile = "sandbox"
# }

variable "sg_id"{
}

variable "amiid" {
    type = string
    default = "ami-0730e12069ab20c26" #got from aws console ec2 =>launch ec2=>choose any
}

resource "aws_instance" "terraform_ec2" {
  ami           = "${var.amiid}"
  instance_type = "t2.micro"
  key_name = "terraform_test" #got from aws console networks&security=>keypairs
  vpc_security_group_ids = ["${var.sg_id}"]
  tags = {
    Name = "Terraform ec2 instance"
  }
}

