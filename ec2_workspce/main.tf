provider "aws" {
    region= "ca-central-1"
    profile = "sandbox"
}

module "sg_module"{
    sg_name = "sg_ec2_${local.env}"
    source ="./sg_module"
}

module "ec2_module" {
    sg_id = "${module.sg_module.sg_id_output}"
    ec2_name = "EC2_Instance_${local.env}"
    source = "./ec2_module"
}

locals {
    env= "${terraform.workspace}"
}