
provider "aws"{    
    region= "ca-central-1"
    profile = "sandbox"
}

resource "aws_iam_role" "sagemaker-role1" {
  name = "sagemaker-role1"
  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "sagemaker.amazonaws.com"
        }
      },
    ]
  })
}


resource "aws_iam_role_policy_attachment" "sagemaker-role-attach" {
    policy_arn = "arn:aws:iam::aws:policy/AmazonSageMakerFullAccess"
    role =aws_iam_role.sagemaker-role1.name
}