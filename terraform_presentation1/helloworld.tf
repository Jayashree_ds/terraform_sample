#select the provide i.e AWS

#1---------------------
# provider "aws" {
#     region= "ca-central-1"
#     access_key="XXXXXX"
#     secret_key="YYYYYYY"
# }

#2---------------------
provider "aws" {
    region= "ca-central-1"
    profile = "sandbox"
}

#########################Variables###########################
#string variables
variable "firststring" {
    type = string
    default = "This is my first string"
}


output "myfirstoutput" {
  value       = "${var.firststring}"
  
}

#Maps variables

variable "mapeg" {
    type = map
    default = {
        "useast":"ami1",
        "euwest":"ami2"
    }
}

output "mapoutput"{
    value = "${var.mapeg["useast"]}"
}

#Lists
variable "grouplist" {
    type=list
    default = ["a","b","c","d","e"]
}

output "listoutput" {
    value = "${var.grouplist[0]}"
}

#Booleans

variable "testbool" {
    default = false
}

output "booloutput" {
    value = "${var.testbool}"
}

######################Input/output Variables#############################

variable "inputvar" {
    type = string
}

output "outputvar" {
    sensitive = true #to hide the sensitive data 
    value ="${var.inputvar}"
}



