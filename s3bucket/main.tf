provider "aws" {
    region= "ca-central-1"
    profile = "sandbox"
}
####################create s3bucket########################
resource "aws_s3_bucket" "mybucket"{
    bucket = "presentation-test-demo"
    acl="private"
    tags={
        Environment="Dev"
    }
}

#upload a file to s3bucket
resource "aws_s3_bucket_object" "firstobject" {
    bucket = "${aws_s3_bucket.mybucket.id}"
    key = "testfile.txt"
    source = "../testfile/sampleobject.txt"
    etag = "${md5(file("../testfile/sampleobject.txt"))}" #used to modify the resources:this is used to keep track of the changes in the files ,if some changesd inside th file tag will be changed so the file will be updated
}

