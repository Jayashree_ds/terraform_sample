provider "aws" {
    region= "ca-central-1"
    profile = "sandbox"
}
variable "vpcid" {
    type=string
    default = "vpc-08192d60" #got form the aws console vpc=>myvpc
}

resource "aws_security_group" "terraform_ec2_sg" {
  name        = "terraform_ec2_sg"
  description = "terraform sg ec2 instances"
  vpc_id      = "${var.vpcid}"

#it means allow port num 22 from everywhere,you  can also specify the IP address
#ssh into this instance , use TCP protocol
  ingress {
    description      = "TLS from VPC"
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
   
  }

  egress {
    from_port        = 22
    to_port          = 22
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]

  }
}


variable "amiid" {
    type = string
    default = "ami-0730e12069ab20c26" #got from aws console ec2 =>launch ec2=>choose any
}

resource "aws_instance" "terraform_ec2" {
  ami           = "${var.amiid}"
  instance_type = "t2.micro"
  key_name = "terraform_test" #got from aws console networks&security=>keypairs
  vpc_security_group_ids = ["${aws_security_group.terraform_ec2_sg.id}"]
  tags = {
    Name = "Terraform ec2 instance"
  }
}
