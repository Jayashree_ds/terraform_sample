provider "aws" {
    region= "ca-central-1"
    profile = "sandbox"
}

module "sg_module"{
    source ="./sg_module"
}

module "ec2_module" {
    sg_id = "${module.sg_module.sg_id_output}"
    source = "./ec2_module"
}
