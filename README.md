
This Repository contains sample codes for setting up the infrastructure using AWS.



Steps To Perform
Install Terraform
create a terraform file 
define the provider inside the script ,its resources and properties

Create s3 bucket and upload a file
https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket

Create Security group 
https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group

Create Ec2 instance
https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance
Presentation slides:
https://docs.google.com/presentation/d/1XyxbGUoL3fyeeK_PkjuDSA13qshgkGlh9onIFiFs_l4/edit?usp=sharing